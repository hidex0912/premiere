# Cours NSI

Ce dépôt Git contient les supports de l'enseignement de __1ère NSI__.

## Découpage

Un chapitre contient des éléments introductifs et historiques à l'enseignement de NSI :

<table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin:auto;">
<tr><td colspan=6 style="text-align: center;"><a title="Introduction" href="./introduction"><img src='./assets/intro.svg' width="128px"/><br/>Introduction</a></td></tr>
</table>

L'enseignement de NSI se décompose en 6 chapitres :

<table cellspacing="0" cellpadding="0" style="border-collapse:collapse; margin:auto;" >
<tbody>
<tr>
<td style="text-align: center;"><a title="Représentation des données : Types et valeurs de base" href="./representation_base"><img src='./assets/binary.svg' width="128px"/><br/>Types et valeurs de base</a></td>
<td style="border: none;text-align: center;"><a title="Représentation des données : Types construits" href="./representation_construite"><img src='./assets/complex.svg' width="128px"/><br/>Types construits</a></td>
<td style="border: none;text-align: center;"><a title="Réseau et Web" href="./reseau"><img src='./assets/network.svg' width="128px"/><br/>Réseau et Web</a></td>
<td style="border: none;text-align: center;"><a title="Architectures matérielles et systèmes d'exploitation" href="./architecture_systeme"><img src='./assets/system.svg' width="128px"/><br/>Architectures matérielles et systèmes d'exploitation</a></td>
<td style="border: none;text-align: center;"><a title="Langages et programmation" href="./programmation"><img src='./assets/python.svg' width="128px"/><br/>Langages et programmation</a></td>
<td style="border: none;text-align: center;"><a title="Algorithmique" href="./algorithmique"><img src='./assets/algorithm.svg' width="128px"/><br/>Algorithmique</a></td>
</tr>
</tbody>
</table>

## Définition

Cet enseignement s'appuie sur quatre concepts :

- Les __données__, qui représentent sous une forme numérique unifiée des informations très diverses,
- Les __algorithmes__, qui spécifient de façon abstraite et précise des traitements à effectuer sur les données,
- Les __langages__, qui permettent de traduire les algorithmes abstraits en programmes exécutables par les machines.
- Les __machines__, et leurs systèmes d'exploitation, qui permettent d'exécuter des programmes en enchaînant un grand nombre d'instructions simples, assurent la persistance des données par leur stockage et de gérer les communications. On y inclut les objets connectés et les réseaux.

## Attendus

- En NSI, on attend des élèves qu'ils sachent faire __preuve d'autonomie, d'initiative et de créativité__ pour face à un problème donné, le décomposer en sous-problèmes, y trouver des solutions, et mettre en œuvre ces solutions notamment via un langage informatique. Il faut savoir faire preuve d'abstraction.
- Ils doivent faire preuve d'__esprit d'équipe__ : l'informatique s'est construite par la collaboration et il est très fréquent de relire et d'améliorer des programmes faits par d'autres personnes.
- La __rigueur et l'organisation__ sont également très importantes : les langages et les protocoles reposent sur des règles très précises qu'il faut savoir suivre.
- Enfin, une bonne partie de l'apprentissage en informatique se fait par soi-même, en lisant des sites et des articles. Il faut savoir __rechercher de l'information et partager des ressources__.

## Évaluation

![Évaluation au bac, Source : letudiant.fr](./assets/notes.png){ width=50% }

2 situations :

1. la spécialité NSI est __abandonnée__ en _Première_ : la note du bac est égale à la __moyenne__ des bulletins scolaires de cette année, __coefficient 8__.
2. la spécialité NSI est __gardée__ en _Terminale_ : la note du bac est égale à l'__épreuve terminale__, __coefficient 16__.

L'évaluation en 1ère :

- __Objectifs__ : Vérifier vos acquis et identifier vos difficultés. Évaluation $`\neq`$ Sanction.
- __Périodicité__ : Tout au long de l'année,
- __Modalité__ : QCM, TD, exposés, projets

## Progression

| Trimestre | Objectifs | Séquence 1               | Séquence 2                 |
| --- | ----- | ------------------------ | -------------------------- |
| 1 | - Qu'est-ce que __coder__ et __programmer__ ?<br />- Qu'est-ce qu'une __donnée__ en machine ? | **Langage de programmation** :<br />  - Affectation<br />  - Structures conditionnelles<br />  - Fonctions<br />  - Boucles bornées et non bornées<br />  - Mutabilité | **Représentation des données : types et valeurs de base** : <br/>  - Numération<br />  - Écriture binaire des entiers relatifs<br />  - Écriture binaire des nombres à virgules flottants<br />  - Conversion de base (binaire, hexadécimal, décimal)<br /><br />**Représentation des données : types construits** : <br />  - List<br />  - Tuple<br />  - Dictionnaire |
| 2 | - Comment __prouver__ un algorithme ?<br />- Qu'est-ce qu'un __système d'exploitation__ ? | **Algorithmique** : <br />  - Terminaison<br />  - Complexité<br />  - Recherche de minimum <br />  - Tri par sélection<br />  - Insertion dans une liste triée<br />  - Tri par insertion <br/> | **Architectures matérielles et systèmes d'exploitation** : <br />  - Modèle Von Neumann<br />  - Langage assembleur<br />  - Définition d'un OS<br />  - Linux et commandes<br /><br />**Représentation des données : types et valeurs de base** :<br />  - Encodage des caractères<br />  - Traitement des données en table (CSV)<br /> |
| 3 | - Comment communique-t-on sur le __WEB__ ?<br/>- Comment __échanger__ des données de manière __sécurisée__ ?<br />- Que se pase-t-il quand on cherche __LA meilleure solution__ ? | **Algorithmique** :<br />  - Dichotomie<br />  - Algorithme Glouton<br />  - Introduction aux algorithmes d’apprentissage<br />  - K plus proches voisins | **Réseau et web**<br />  - Pages Web statiques<br />  - Interactions utilisateur-client (événements en JS)<br />  - Interactions client-serveur (requêtes GET, POST)<br />  - Protocole IP et adressage<br/>  - Les modèles OSI et TCP/IP<br/>  - architecture et routage |
