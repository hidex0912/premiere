---
title : Introduction
author : M. BODDAERT
license : CC-BY-NC-SA
---
# Introduction à la NSI

Ce dossier contient :

- La présentation effectuée lors de la première séance sous la forme d'un PDF ([la présentation](./PRESENTATION.pdf)),
- L'[activité 1](./ACTIVITE_1.md) sur une définition et brève histoire de l'informatique.
- L'[activité 2](./ACTIVITE_2) sur l'apprentissage du langage de balisage Markdown.

